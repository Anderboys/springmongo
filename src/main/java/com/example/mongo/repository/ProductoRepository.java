package com.example.mongo.repository;

import com.example.mongo.document.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
@Repository
public interface ProductoRepository extends MongoRepository<Producto, Serializable> {


}
