package com.example.mongo.repository;

import com.example.mongo.document.Persona;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.io.Serializable;
import java.util.List;

@Repository
@CrossOrigin(value = {})

public interface PersonaRepository extends MongoRepository<Persona, Serializable> {

        @RestResource(path = "apellido",rel = "apellido")
        public List<Persona> findByApellidoOrderByNombre(@Param("apellido") String apellido);

        @RestResource(path = "borrarPorNombre",rel = "borrarPorNombre")
        public long deleteByNombre(@Param("nombre")String nombre);


}
