package com.example.mongo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoApplication {

	private static final Log LOG = LogFactory.getLog(MongoApplication.class);
	public static void main(String[] args) {
		LOG.info("Principal: MongoApplication" );
		SpringApplication.run(MongoApplication.class, args);
	}

}
